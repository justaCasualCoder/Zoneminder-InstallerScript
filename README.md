# Zoneminder Install Script
This repository contains a script to install Zoneminder and a [LAMP](https://en.wikipedia.org/wiki/LAMP_(software_bundle)) Stack on the following linux distributions - 
- Alpine Linux
- Arch Linux
- Ubuntu Linux
- Android ( [Termux](https://termux.dev/) Debian Proot - [My Install Scripts](https://github.com/justaCasualCoder/Zoneminder-Termux) )
- Debian Linux
- Fedora Linux
- OpenSuSE TumbleWeed

Currently the most stable distro to install on is : **Debian**

If you would like a support for a Linux distro , create a issue and i will try to add support
